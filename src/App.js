import React, { Component } from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { Grid, Row, Col } from "react-flexbox-grid";
import Paper from "@material-ui/core/Paper";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import teal from '@material-ui/core/colors/teal';
import pink from '@material-ui/core/colors/pink';
import logo from './logo.svg';

import LocationListContainer from './containers/LocationListContainer';

import './App.css';
import ExtendedForecastContainer from './containers/ExtendedForecastContainer';

const cities = [
  "Villanueva,hn",
  "Coroatá, br",
  "San Pedro Sula,hn",
  "Potrerillos,hn"
];

const theme = createMuiTheme({
  palette: {
    primary: teal,
    secondary: pink
  }
});

class App extends Component {

  render() {
    return (
      <MuiThemeProvider theme={theme}>
          <AppBar position="static">
            <Toolbar>
              <IconButton className="menuButton" color="inherit" aria-label="Menu">
                <img src={logo} className="App-logo" alt="logo" />
              </IconButton>
              <Typography variant="title" color="inherit" className="titleFlex">
                Reactive Weather
              </Typography>
            </Toolbar>
          </AppBar>
          <Grid>
            <Row>
              <Col xs={12} md={6}>
                <LocationListContainer cities={cities}/>
              </Col>
              <Col xs={12} md={6}>
                <Paper zdepth={4}>
                  <div className="detail">
                      <ExtendedForecastContainer city={"Villanueva,hn"}/>
                  </div>
                </Paper>
              </Col>
            </Row>
          </Grid>
      </MuiThemeProvider>
    );
  }
}

export default App;
