import React from 'react';
import PropTypes from "prop-types";
import ForecastItem from "./ForecastItem";
import "./styles.css";

const renderForecastItems = (forecastData) => {
    return forecastData.map( forecast => (
        <ForecastItem
            key={`${forecast.weekDay}${forecast.hour}`}
            weekDay={forecast.weekDay}
            hour={forecast.hour}
            data={forecast.data} />
    ) );
}

const renderProgress = () => {
    return <h3>Loading extended forecast...</h3>
}

const ExtendedForecast = ({city, forecastData}) => {
    return (
        <div>
            <h2 className="forecast-title">
                {`Extended forecast for: ${city} `}
            </h2>
            { forecastData ? renderForecastItems(forecastData) : renderProgress() }
        </div>
    );
}

ExtendedForecast.propTypes = {
    city: PropTypes.string.isRequired,
    forecastData: PropTypes.array,
}
export default ExtendedForecast;

