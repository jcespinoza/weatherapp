import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { getForecastDataFromCities, getCity } from "./../reducers";
import ExtendedForecast from "./../components/ExtendedForecast";

class ExtendedForecastContainer extends Component {
    
    render() {
        const {city, forecastData} = this.props;
        return (
            city && <ExtendedForecast city={city} forecastData={forecastData}/>
        );
    }
}

ExtendedForecastContainer.propTypes = {
    city: PropTypes.string.isRequired
};

const mapStateToProps = (state) => ({city: getCity(state), forecastData: getForecastDataFromCities(state)})

export default connect(mapStateToProps,null)(ExtendedForecastContainer);
