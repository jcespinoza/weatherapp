import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getWeatherCities, getCity } from "./../reducers";
import * as actions from "./../actions";
import LocationList from "./../components/LocationList";

class LocationListContainer extends Component {
    componentDidMount = () => {
        const { setWeather, setSelectedCity, cities, city } = this.props;
        setWeather(cities);
        setSelectedCity(city)
    }

    handleSelectedLocationChange = city =>{   
        this.props.setSelectedCity(city);
    }

    render() {
        const { citiesWeather } = this.props;
        return (
            <LocationList cities={citiesWeather} onSelectedLocation={this.handleSelectedLocationChange}/>
        );
    }
}

LocationListContainer.propTypes = {
    setSelectedCity: PropTypes.func.isRequired,
    setWeather: PropTypes.func.isRequired,
    cities: PropTypes.array.isRequired,
    citiesWeather: PropTypes.array,
    city: PropTypes.string.isRequired,
};

const mapDispatchToPropsAction = (dispatch) => bindActionCreators(actions, dispatch); 

const mapStateToProps = state => ({
    citiesWeather: getWeatherCities(state),
    city: getCity(state)
});
export default connect(mapStateToProps, mapDispatchToPropsAction)(LocationListContainer);
