import transformForecast from '../services/transformForecast';
import transformWeather from '../services/transformWeather';

const openWeather_forecastUrl = "https://api.openweathermap.org/data/2.5/forecast";
const openWeather_weatherUrl = "https://api.openweathermap.org/data/2.5/weather";
const apiKey = "60d2a29cdec0e5a1ec78f03487672702";

export const SET_CITY = "SET_CITY";
export const SET_FORECAST_DATA = "SET_FORECAST_DATA"
export const SET_WEATHER_CITY = "SET_WEATHER_CITY";
export const GET_WEATHER_CITY = "GET_WEATHER_CITY";

const setCity = payload => ({ type: SET_CITY, payload });
const setForecastData = payload => ({ type: SET_FORECAST_DATA, payload });

const getWeatherCity = payload => ({type: GET_WEATHER_CITY, payload});
const setWeatherCity = payload => ({type: SET_WEATHER_CITY, payload});

export const setSelectedCity = payload => {
    return (dispatchFunction, getState) => {
        const url_forecast = `${openWeather_forecastUrl}?q=${payload}&appid=${apiKey}`;

        dispatchFunction(setCity(payload));

        const oldState = getState();
        const date = oldState.cities[payload] && oldState.cities[payload].forecastDataDate;
        const now = new Date();
        if(date && (now < date) < 1 * 60 * 1000){
            return;
        }

        return fetch(url_forecast)
        .then(response => (response.json()))
        .then(wData => {
            const forecastData = transformForecast(wData);
            
            dispatchFunction(setForecastData({city: payload, forecastData}))
        });
    }
};

export const setWeather = payload => {
    return dispatchFunction => {
        payload.forEach(city => {
            dispatchFunction(getWeatherCity(city));
            const apiURL = `${openWeather_weatherUrl}?q=${city}&appid=${apiKey}`;
    
            fetch(apiURL).then( data => {
                return data.json();
            }).then(weather_data => {
                const weather = transformWeather(weather_data);

                dispatchFunction(setWeatherCity({city, weather}));
            });
        })
    }
}